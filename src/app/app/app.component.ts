import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  items = [
    {name: 'item1', price: 100},
    {name: 'item2', price: 200},
    {name: 'item3', price: 300},
    {name: 'item4', price: 400},
    {name: 'item5', price: 500},
    {name: 'item6', price: 600},
    {name: 'item7', price: 700},
    {name: 'item8', price: 800},
    {name: 'item9', price: 900},
    {name: 'item10', price: 1000},
    {name: 'item11', price: 1100},
    {name: 'item12', price: 1200},
    {name: 'item13', price: 1300},
    {name: 'item14', price: 1400},
    {name: 'item15', price: 1500},
    {name: 'item16', price: 1600},
    {name: 'item17', price: 1700},
    {name: 'item18', price: 1800},
    {name: 'item19', price: 1900},
    {name: 'item20', price: 2000},
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
